program functionOverloading;

function a1():integer;
var q:integer;
begin
  q := 1;
end;

function a1():integer;
var q1:integer;
begin
  q1 := 1;
end;

var a:integer;
begin
  a:=1;
end.