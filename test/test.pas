program test1;
type 
	abv = integer;
var
	array1 : array [1..10,1..100,34..56,45..56] of cardinal;
var
	array0 : array [1..20] of cardinal;
	a1,a3,a2 : abv;
	abg:integer;

type 
   Rectangle = object  
   private  
      length, width: integer; 
   public  
      constructor init(l, w: integer);
      destructor done();
      function setlength(l: integer):integer;
      function getlength(): integer;  
      function setwidth(w: integer):integer;  
      function getwidth(): integer;  
      function draw():integer;
end;

constructor Rectangle.init(l, w: integer);
begin
   length := l;
   width := w;
end;

destructor Rectangle.done();
begin
   write(' Desctructor Called');
end; 

function Rectangle.setlength(l: integer):integer;
begin
   length := l;
end;

function Rectangle.setwidth(w: integer):integer;
begin
   width :=w;
end;

function Rectangle.getlength(): integer;  
begin
   getlength := length;
end;

function Rectangle.getwidth(): integer;  
begin
   getwidth := width;
end;

function Rectangle.draw():integer;
var 
   i, j: integer;
begin
   for i:= 1 to length do
   begin
      for j:= 1 to width do
         write(' * ');
   end;
end;
function average(a,b,c:integer): integer;   
var
    i, sum : integer;

function avg(a1:cardinal):cardinal;
	const 
		cvb = 23;
	
	begin
		a := 4451;
		abg:=cvb;
	end;

var car: cardinal;

begin
    sum := 6546;
    car := 2343;
    average := sum;
    sum := avg(car);
    a := 34;
end;

var 
	b,c,d,h,g: int64;
	pointer: ^extended;
type
	enum1 = (das,das22,dsad,sad:=9,sads,sdads,ad);
	enum2 = (das1,das221,dsad1,sad1,sads1,sdad1s,a1d);
	array0 = array [1..10,1..100,34..56,45..56] of integer;
const
	a123 = 3;
	zxc = a123 + 12;
var
	a:extended;
	k:abv;
begin 
	a := 1;
	a := average(integer(2),integer(3),integer(4));
	h := g + 5;
	array1[2,10,h,4] := 5;
	a := 2*8.5-78*54;
	a := 12.9;
	pointer := @a;
	pointer^ := 45;
end.