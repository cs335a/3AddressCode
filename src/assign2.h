extern int lineNo;
extern char* yytext;
extern int yyleng;
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctype.h>
#include <utility> 
#include "fstream"
#include <map>
#include <list>
#include <string.h>
#include <string>
#include <stack>
#define LLI long long int
using namespace std;
struct arrayRange;
struct tupleNew;
struct symTable;
struct typeName;
struct argument;
struct ThreeAC_src;
typedef list<int>* List;
typedef list<int> listType;
typedef list<ThreeAC_src> paramList;
typedef pair<string,typeName> typePair;
typedef map<string,typeName> typeMap;
typedef list<arrayRange> arrayRangeList;
typedef list<argument> argumentList;
typedef list<tupleNew*> tupleList;
enum opType {plusOp,minusOp,minusUniOp,multiplyOp,divideOp,assignOp,orOp,andOp,notOp,lessOp,grOp,equalOp,gotoOp,labelOp,exitOp,lessEqualOp,grEqualOp,inOp,ssOp,intDivideOp,modOp,asOp,shiftLeftOp,shiftRightOp,notEqualOp,isOp,xorOp, loadOp, storeOp, addrOp,returnOp,callOp,paramOp,dotOp,newOp,disposeOp, trueOp, falseOp, ifOp, readOp, writeOp}; //newOp dest-value in which return pointer is stored, src1-- jump target src2--type info 
enum switchAC {ACnil, ACint,ACchar,ACdouble,ACtuple,ACnone,AClabel,ACstr,ACtemp,ACbool };
enum errorCategory {notConstant,enumIndexOverflow, redefineError,redefineType,undefinedVar,undefinedType,typeMismatch,rangeUnderflow,arrayIndexOverflow,InvalidFunctionDefinition,InvalidConstructorDefinition,InvalidDestructorDefinition,structuredTypeInDeclaration,unknownObjectField,notObject,constantAssignment};
struct arrayRange{
	int start;
	int size;
};
struct typeName{
    int numBytes;
    char *name;
    int id;
    int level;
    int length;
    arrayRangeList* rangeList;
    bool hasRangeList;
    symTable* childTable;
    bool isObject;
};
struct tupleNew{
	typeName type;
	int offset;
	int startIndex; 
	symTable* childTable = NULL;
    string name;
    argumentList* argList;
    bool isConstant ;
    bool isPrivate;
};
struct symTable{
	map<string,tupleNew*> table;
	int offset;
	symTable* parent = NULL;
	typeMap* typeList = NULL;
    tupleNew* returnVar = NULL;
};
struct ThreeAC_src{
    tupleNew* tupleSrc;
    LLI intSrc;
    char* strSrc ;
    char charSrc;
    double doubleSrc;
    bool boolSrc;
    switchAC multiplexer;
    typeName type;
};
struct threeAC{
    ThreeAC_src src1;
    ThreeAC_src src2;
    ThreeAC_src dest;
    opType op;
};
struct nonTerminalAttr{
    threeAC code;
	typeName type;
	char* name;
    List trueList;
    List falseList;
    bool isConstant;
    bool isConstantInt;
    bool isComp;
    bool isPointer;
    bool isAndOr;
    int index;
    bool isPrivate;

};
struct loopStackElem{
    List breakList;
    List continueList;
};
struct argument{
    typeName type;
    char* name;
};
tupleNew* findTuple(char* name);
typeName findType(char* name);
void throwError(errorCategory error , char* name);
void throwError(errorCategory error);